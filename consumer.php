<?php

require_once __DIR__ .'/vendor/autoload.php';

class Consumer extends \Evenement\EventEmitter
{
    /** @var \React\Amqp\Connection */
    private $connection;

    /** @var \React\Amqp\Channel */
    private $channel;

    /** @var \React\EventLoop\LoopInterface */
    private $loop;

    public function __construct()
    {
        $loop = React\EventLoop\Factory::create();
        $resolverFactory = new \React\Dns\Resolver\Factory();
        $resolver = $resolverFactory->create('8.8.8.8', $loop);
        $connector = new \React\Amqp\SocketConnector($loop, $resolver);

        $this->connection = new \React\Amqp\Connection($loop, $connector);
        $this->loop = $loop;

        $this->on('channelCreated', array($this, 'handleChannelCreated'));
    }

    public function run()
    {
        $start = microtime(true);
        $that = $this;
        $this->connection->on('connected', function() use ($start, $that) {
                $end = microtime(true) - $start;
                echo "Connected to RabbitMQ in $end sec\n";

                $that->connection->createChannel()->then(function($channel) {
                        $this->emit('channelCreated', array($channel));
                    });
            });

        $this->connection->connect('localhost', 5672, 'guest', 'guest');
        $this->loop->run();
    }

    public function handleChannelCreated($channel)
    {
        $this->channel = $channel;
        echo "Channel created\n";
    }
}


$consumer = new Consumer();
$consumer->run();
