<?php


namespace React\Amqp\Tests;


use React\Amqp\UnsignedLongLong;

class UnsignedLongLongTest extends TestCase
{
    public function testFromInt()
    {
        $longLong = UnsignedLongLong::fromInt(-43951789);
        $this->assertInstanceOf('React\Amqp\UnsignedLongLong', $longLong);
        $this->assertBcEquals('18446744073665599827', (string) $longLong);
    }

    public function testFromHiAndLoWords()
    {
        $value = UnsignedLongLong::fromHiAndLoWords(500, 12345);
        $expected = '2147483660345';
        $this->assertBcEquals($expected, $value);
    }

    public function testOutOfBounds()
    {
        $this->assertTrue(UnsignedLongLong::outOfBounds('-1'));
        $this->assertTrue(UnsignedLongLong::outOfBounds('18446744073709551616'));
        $this->assertFalse(UnsignedLongLong::outOfBounds('0'));
        $this->assertFalse(UnsignedLongLong::outOfBounds('18446744073709551615'));
    }

    public function testToBinaryString()
    {
        $longLong = UnsignedLongLong::fromInt(-43951789);
        $this->assertEquals("\xFF\xFF\xFF\xFF\xFD\x61\x59\x53", $longLong->toBinaryString());
    }
}
