<?php
namespace React\Amqp\Tests;

use React\Amqp\Decimal;
use React\Amqp\UnsignedLong;
use React\Amqp\ValueWriter;

class ValueWriterTest extends \PHPUnit_Framework_TestCase
{
    public function testWriteBit()
    {
        $writer = new ValueWriter();
        $result = $writer->writeBit(true)
            ->writeBit(0)
            ->writeBit(1)
            ->writeBit('asd')
            ->getResult();
        /** 1101 => dec 13 => hex D */
        $this->assertEquals("\xD", $result);
    }

    public function testWriteOctet()
    {
        $writer = new ValueWriter();
        $result = $writer->writeOctet(157)->getResult();
        $this->assertEquals("\x9D", $result);
    }

    public function testWriteShort()
    {
        $writer = new ValueWriter();
        $result = $writer->writeShort(40296)->getResult();
        $this->assertEquals("\x9D\x68", $result);
    }

    public function testWriteLongForValidArgument()
    {
        $writer = new ValueWriter();

        $result = $writer->writeUnsignedLong(2452)
            ->writeUnsignedLong('6372')
            ->writeUnsignedLong(new UnsignedLong(2451))
            ->getResult();
        $this->assertEquals("\x00\x00\x09\x94\x00\x00\x18\xE4\x00\x00\x09\x93", $result);
    }

    /**
     * @dataProvider invalidLongs
     */
    public function testWriteLongForInvalidArgument($value)
    {
        $this->setExpectedException('InvalidArgumentException');
        $writer = new ValueWriter();
        $writer->writeUnsignedLong($value);
    }

    public function invalidLongs()
    {
        return array(
            array('76asf3e'),
            array(true),
            array(3434.23),
            array(array(431412))
        );
    }

    public function testWriteUnsignedLongLong()
    {
        $writer = new ValueWriter();
        $result = $writer->writeUnsignedLongLong('28367452376523')->getResult();
        $this->assertEquals("\x00\x00\x19\xCC\xCF\xEC\x0D\xCB", $result);
    }

    public function testWriteShortString()
    {
        $writer = new ValueWriter();
        $result = $writer
            ->writeShortString('examplestring')
            ->writeShortString('foobar')
            ->getResult();
        $this->assertEquals("\x0Dexamplestring\x06foobar", $result);
    }

    public function testWriteLongString()
    {
        $writer = new ValueWriter();
        $result = $writer
            ->writeLongString('examplestring')
            ->writeLongString('another')
            ->getResult();
        $this->assertEquals("\x00\x00\x00\x0Dexamplestring\x00\x00\x00\x07another", $result);
    }

    public function testWriteSignedLong()
    {
        $writer = new ValueWriter();
        $result = $writer
            ->writeSignedLong(-32863)
            ->writeSignedLong(123765)
            ->getResult();
        $this->assertEquals("\xFF\xFF\x7F\xA1\x00\x01\xE3\x75", $result);
    }

    public function testWriteDecimal()
    {
        $writer = new ValueWriter();
        $result = $writer
            ->writeDecimal(new Decimal(-4321, 2))
            ->writeDecimal(new Decimal(7319346, 3))
            ->getResult();

        $this->assertEquals("\x02\xFF\xFF\xEF\x1F\x03\x00\x6F\xAF\x32", $result);
    }

    public function testWriteTable()
    {
        $table = array(
            'SignedLong' => array('I', -84632),
            'LongString' => array('S', 'sometext'),
            'Decimal' => array('D', new Decimal(12345, 2)),
        );

        $writer = new ValueWriter();
        $result = $writer
            ->writeTable($table)
            ->getResult();

        $this->assertEquals("\x00\x00\x00\x36\x0ASignedLongI\xFF\xFE\xB5\x68\x0ALongStringS\x00\x00\x00\x08sometext\x07DecimalD\x02\x00\x00\x30\x39", $result);
    }
}
