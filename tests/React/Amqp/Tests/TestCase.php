<?php
namespace React\Amqp\Tests;

class TestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * PHPUnit built-in assertEquals doesn't work for 64-bit integers. That is why this method exists.
     *
     * @param $expected
     * @param $actual
     * @param string $message
     * @throws \PHPUnit_Framework_ExpectationFailedException
     */
    public function assertBcEquals($expected, $actual, $message = '')
    {
        try {
            $this->assertTrue(bccomp($expected, $actual) == 0);
        } catch (\PHPUnit_Framework_ExpectationFailedException $e) {
            if (!$message) {
                $message = "Failed asserting that $expected equals to $actual";
            }
            throw new \PHPUnit_Framework_ExpectationFailedException($message, $e->getComparisonFailure());
        }

    }
}
