<?php
namespace React\Amqp\Tests;

use React\Amqp\Frame;

class FrameTest extends \PHPUnit_Framework_TestCase
{
    public function testReadFromStream()
    {
        $streamMock = $this->getMock('\React\Amqp\Stream', array(), array(), '', false);
        $streamMock->expects($this->any())
            ->method('read')
            ->will($this->onConsecutiveCalls("\x01", "\x00\x00", "\x00\x00\x00\x02", "\xAB\xCD", "\xCE"));

        $frame = Frame::readFromStream($streamMock);

        $this->assertEquals(Frame::TYPE_METHOD, $frame->getType());
        $this->assertEquals(0, $frame->getChannel());
        $this->assertEquals(2, $frame->getSize());
        $this->assertEquals("\xAB\xCD", $frame->getPayload());
    }

    public function testReadFromStreamThrowsException()
    {
        $this->setExpectedException('\RuntimeException', 'Invalid frame.');

        $streamMock = $this->getMock('\React\Amqp\Stream', array(), array(), '', false);
        $streamMock->expects($this->any())
            ->method('read')
            ->will($this->onConsecutiveCalls("\x01", "\x00\x00", "\x00\x00\x00\x02", "\xAB\xCD", "\x00"));

        Frame::readFromStream($streamMock);
    }

    public function testToBinaryString()
    {
        $frame = new Frame(Frame::TYPE_METHOD, 0, "\xAB\xCD\xEF");
        $actual = $frame->toBinaryString();

        $this->assertEquals("\x01\x00\x00\x00\x00\x00\x03\xAB\xCD\xEF\xCE", $actual);
    }

    public function testGetSizeForPayloadChanges()
    {
        $frame = new Frame(Frame::TYPE_METHOD, 0, "\xAB\xCD\xEF");
        $this->assertEquals(3, $frame->getSize());

        $frame->setPayload("\x00\x01\x02\x03\x04");
        $this->assertEquals(5, $frame->getSize());
    }
}
