<?php
namespace React\Amqp\Tests;

use React\Amqp\Decimal;

class DecimalTest extends \PHPUnit_Framework_TestCase
{
    public function testToString()
    {
        $dec = new Decimal(12345, 2);
        $this->assertEquals('123.45', (string) $dec);
    }

    public function testToBinaryString()
    {
        $dec = new Decimal(12345, 3);
        $this->assertEquals("\x03\x00\x00\x30\x39", $dec->toBinaryString());
    }
}
