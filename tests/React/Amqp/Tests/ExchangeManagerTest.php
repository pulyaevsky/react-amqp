<?php
namespace React\Amqp\Tests;

use React\Amqp\Exchange;
use React\Amqp\ExchangeManager;

class ExchangeManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $connectionMock;

    private $channelMock;

    public function setUp()
    {
        $this->connectionMock = $this->getMock('\React\Amqp\Connection', array(), array(), '', false);
        $this->channelMock = $this->getMock('\React\Amqp\Channel', array(), array(), '', false);
    }

    public function testGetExchangeByNameThrowsExceptionIfNotFound()
    {
        $this->setExpectedException('\RuntimeException', "Exchange with name 'test' not found.");
        $manager = new ExchangeManager($this->channelMock, $this->connectionMock);
        $manager->getExchangeByName('test');
    }

    public function testDeclareExchange()
    {
        $this->connectionMock->expects($this->once())
            ->method('sendFrame');
        $manager = new ExchangeManager($this->channelMock, $this->connectionMock);
        $manager->declareExchange('x-my.exchange', Exchange::TYPE_DIRECT, false, false, true, array());
    }
}
