<?php
namespace React\Amqp\Tests;

use React\Amqp\SignedLong;

class SignedLongTest extends \PHPUnit_Framework_TestCase
{
    public function testIntIsInBounds()
    {
        $this->assertFalse(SignedLong::outOfBounds(2147483647));
        $this->assertFalse(SignedLong::outOfBounds(-2147483648));
    }

    public function testIntIsOutOfBounds()
    {
        $this->assertTrue(SignedLong::outOfBounds('2147483648'));
        $this->assertTrue(SignedLong::outOfBounds('-2147483649'));
    }
}
