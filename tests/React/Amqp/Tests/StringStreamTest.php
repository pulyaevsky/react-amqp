<?php
namespace React\Amqp\Tests;

use React\Amqp\StringStream;

class StringStreamTest extends \PHPUnit_Framework_TestCase
{
    public function testRead()
    {
        $stream = new StringStream('teststring');
        $result = $stream->read(2);
        $this->assertEquals('te', $result);
        $result = $stream->read(2);
        $this->assertEquals('st', $result);
        $result = $stream->read(6);
        $this->assertEquals('string', $result);
    }

    public function testReadOutOfBounds()
    {
        $this->setExpectedException('OutOfBoundsException');
        $stream = new StringStream('test');
        $stream->read(5);
    }

    public function testEof()
    {
        $stream = new StringStream('teststring');
        $this->assertFalse($stream->eof());
        $stream->read(5);
        $this->assertFalse($stream->eof());
        $stream->read(5);
        $this->assertTrue($stream->eof());
    }
}
