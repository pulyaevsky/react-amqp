<?php
namespace React\Amqp\Tests;


use React\Amqp\UnsignedLong;

class UnsignedLongTest extends \PHPUnit_Framework_TestCase
{
    public function testConvertNegativeIntToUnsigned()
    {
        $this->assertEquals('4251015507', UnsignedLong::convertNegativeIntToUnsigned(-43951789));
    }

    public function testFromInt()
    {
        $long = UnsignedLong::fromInt(-43951789);
        $this->assertInstanceOf('React\Amqp\UnsignedLong', $long);
        $this->assertEquals('4251015507', (string) $long);
    }

    public function testFromIntOutOfBounds()
    {
        $this->setExpectedException('OutOfBoundsException', 'Integer \'-2147483649\' is out of bounds. Must be between -2147483648 and 2147483647.');
        UnsignedLong::fromInt('-2147483649');
    }

    public function testToBinaryString()
    {
        $long = UnsignedLong::fromInt(-43951789);
        $this->assertEquals("\xFD\x61\x59\x53", $long->toBinaryString());
    }
}
