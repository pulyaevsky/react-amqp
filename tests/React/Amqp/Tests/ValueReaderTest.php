<?php


namespace React\Amqp\Tests;


use React\Amqp\Decimal;
use React\Amqp\Stream;
use React\Amqp\UnsignedLong;
use React\Amqp\ValueReader;

class ValueReaderTest extends TestCase
{
    private static $resources = array();

    public static function tearDownAfterClass()
    {
        foreach (self::$resources as $resource) {
            fclose($resource);
        }
    }

    public function testReadBit()
    {
        $stream = $this->createStreamForFixture('readbit.data');
        $reader = new ValueReader($stream);
        $this->assertEquals(1, $reader->readBit());
        $this->assertEquals(0, $reader->readBit());
        $this->assertEquals(0, $reader->readBit());
        $this->assertEquals(1, $reader->readBit());
    }

    private function createStreamForFixture($fixtureName)
    {
        $resource = fopen(__DIR__ .'/Fixtures/'. $fixtureName, 'r');
        self::$resources[] = $resource;
        $mock = $this->getMock('React\EventLoop\StreamSelectLoop');

        return new Stream($resource, $mock);
    }

    public function testReadOctet()
    {
        $stream = $this->createStreamForFixture('readoctet.data');
        $reader = new ValueReader($stream);
        $this->assertEquals(146, $reader->readOctet());
    }

    public function testReadShort()
    {
        $stream = $this->createStreamForFixture('readshortint.data');
        $reader = new ValueReader($stream);
        $this->assertEquals(63455, $reader->readShort());
    }

    public function testReadLong()
    {
        $stream = $this->createStreamForFixture('readlongint.data');
        $reader = new ValueReader($stream);
        $long = $reader->readUnsignedLong();
        $this->assertInstanceOf('React\Amqp\UnsignedLong', $long);
        $this->assertEquals('43951789', $long);
        $this->assertEquals('4251015511', $reader->readUnsignedLong()); // -43951785 in signed int
    }

    public function testShortString()
    {
        $stream = $this->createStreamForFixture('readshortstring.data');
        $reader = new ValueReader($stream);
        $result = $reader->readShortString();
        $this->assertEquals('first', $result);
        $result = $reader->readShortString();
        $this->assertEquals('second', $result);
    }

    public function testReadLongString()
    {
        // We don't test here really LONG strings since it will use inappropriate amount of memory for tests.
        // We'll just check that string is unpacked correctly.
        $stream = $this->createStreamForFixture('readlongstring.data');
        $reader = new ValueReader($stream);
        $result = $reader->readLongString();
        $this->assertEquals('first', $result);
        $result = $reader->readLongString();
        $this->assertEquals('second', $result);
    }

    public function testReadSignedLong()
    {
        $stream = $this->createStreamForFixture('readsignedlong.data');
        $reader = new ValueReader($stream);
        $result = $reader->readSignedLong();
        $this->assertEquals(-34587, $result);
    }

    public function testReadDecimal()
    {
        $stream = $this->createStreamForFixture('readdecimal.data');
        $reader = new ValueReader($stream);
        $result = $reader->readDecimal();
        $this->assertEquals('-345.87', (string) $result);
        $result = $reader->readDecimal();
        $this->assertEquals('654.321', (string) $result);
    }

    public function testReadUnsignedLongLong()
    {
        $stream = $this->createStreamForFixture('readunsignedlonglong.data');
        $reader = new ValueReader($stream);
        $result = $reader->readUnsignedLongLong();
        $expected = '18446744073665599827';
        $this->assertBcEquals($expected, $result);
    }

    public function testReadTable()
    {
        $stream = $this->createStreamForFixture('readtable.data');
        $reader = new ValueReader($stream);
        $result = $reader->readTable();
        $this->assertTrue(is_array($result));
        $this->assertArrayHasKey('lString', $result);
        $this->assertEquals(array('S', 'second'), $result['lString']);
        $this->assertArrayHasKey('lSignedInt', $result);
        $this->assertEquals(array('I', '45647896'), $result['lSignedInt']);
        $this->assertArrayHasKey('Decimal', $result);
        $this->assertEquals(array('D', new Decimal(-34587, 2)), $result['Decimal']);
    }
}
