<?php
namespace React\Amqp\Tests;

use React\Amqp\Connection;

class ConnectionTest extends \PHPUnit_Framework_TestCase
{
    private $connector;

    public function testGetNextAvailableChannelId()
    {
        $connection = $this->createConnection();
        $this->assertEquals(1, $connection->getNextAvailableChannelId());
        $this->assertEquals(2, $connection->getNextAvailableChannelId());
        $this->assertEquals(3, $connection->getNextAvailableChannelId());
    }

    private function createConnection()
    {
        $loop = $this->getMock('React\EventLoop\StreamSelectLoop');
        $this->connector = $this->getMock('React\Amqp\SocketConnector', array(), array(), '', false);
        $connection = new Connection($loop, $this->connector);

        return $connection;
    }

    public function testGetNextAvailableChannelIdThrowsOutOfBoundsException()
    {
        $this->setExpectedException('OutOfBoundsException');
        $connection = $this->createConnection();
        $connection->setChannelMax(2);
        $connection->getNextAvailableChannelId();
        $connection->getNextAvailableChannelId();
        $connection->getNextAvailableChannelId();
    }
}
