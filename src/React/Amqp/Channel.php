<?php
namespace React\Amqp;

use Evenement\EventEmitter;
use React\Amqp\Method\ChannelClose;
use React\Amqp\Method\ChannelCloseOk;
use React\Amqp\Method\ChannelFlow;
use React\Amqp\Method\ChannelFlowOk;
use React\Amqp\Method\ChannelOpen;
use React\Amqp\Method\ChannelOpenOk;
use React\Amqp\Method\MethodFactory;
use React\Promise\Deferred;

class Channel extends EventEmitter
{
    private $id;

    private $connection;

    private $active = true;

    private $opened = false;

    private $declaredExchanges = array();

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->id = $connection->getNextAvailableChannelId();
    }

    public function getId()
    {
        return $this->id;
    }

    public function handleFrame(Frame $frame)
    {
        switch ($frame->getType()) {
            case Frame::TYPE_METHOD:
                $method = MethodFactory::createFromFrame($frame);
                $this->emit($method->getName(), array($method));
                break;
            default:
                throw new \RuntimeException("Can't handle frame.");
                break;
        }
    }

    public function open()
    {
        if (!$this->opened) {
            $this->on('channel.open-ok', array($this, 'handleOpenOk'));
            $open = new ChannelOpen();
            $frame = Frame::createFromClientMethod($open, $this->id);
            $this->connection->sendFrame($frame);
        }
    }

    public function handleOpenOk(ChannelOpenOk $method)
    {
        $this->opened = true;
        $this->on('channel.flow', array($this, 'handleFlow'));
        $this->on('channel.flow-ok', array($this, 'handleFlowOk'));
        $this->on('channel.close', array($this, 'handleClose'));
        $this->on('channel.close-ok', array($this, 'handleCloseOk'));

        $this->emit('open', array($this));
    }

    public function activate()
    {
        return $this->requestFlowChange(true);
    }

    public function deactivate()
    {
        return $this->requestFlowChange(false);
    }

    protected function requestFlowChange($value)
    {
        $deferred = new Deferred();
        $this->once('internalFlowChanged', function($active) use ($deferred) {
                $deferred->resolve($active);
            });

        $flow = new ChannelFlow($value);
        $frame = Frame::createFromClientMethod($flow);
        $this->connection->sendFrame($frame);

        return $deferred->promise();
    }

    public function handleFlow(ChannelFlow $flow)
    {
        $this->active = $flow->getActive();
        $flowOk = new ChannelFlowOk($this->active);
        $frame = Frame::createFromClientMethod($flowOk);
        $this->connection->sendFrame($frame);
        $this->emit('flowChanged', array($this->active));
    }

    public function handleFlowOk(ChannelFlowOk $flowOk)
    {
        $this->active = $flowOk->getActive();
        $this->emit('internalFlowChanged', array($flowOk->getActive()));
    }

    public function handleClose(ChannelClose $close)
    {
        $closeOk = new ChannelCloseOk();
        $frame = Frame::createFromClientMethod($closeOk);
        $this->connection->sendFrame($frame);
        $this->emit('closed');
    }

    public function handleCloseOk(ChannelCloseOk $closeOk)
    {
        $this->emit('closed');
    }

    public function declareExchange($exchangeName, $type, $passive, $durable, $noWait, $arguments, $autoDelete = true, $internal = false)
    {

    }
}
