<?php
namespace React\Amqp;

class SignedLong
{
    const MIN = '-2147483648';
    const MAX = '2147483647';

    public static function outOfBounds($value)
    {
        $value = (string) $value;
        if (bccomp($value, self::MIN) == -1 || bccomp($value, self::MAX) == 1) {
            return true;
        }

        return false;
    }
}
