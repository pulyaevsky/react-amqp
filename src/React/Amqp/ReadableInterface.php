<?php
namespace React\Amqp;

interface ReadableInterface
{
    public function read($bytesCount);
    public function eof();
}
