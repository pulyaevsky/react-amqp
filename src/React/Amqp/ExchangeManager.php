<?php
namespace React\Amqp;

class ExchangeManager
{
    /** @var Channel */
    private $channel;

    /** @var Connection */
    private $connection;

    private $exchanges = array();

    public function __construct(Channel $channel, Connection $connection)
    {
        $this->channel = $channel;
        $this->connection = $connection;
    }

    public function getExchangeByName($name)
    {
        if (!isset($this->exchanges[$name])) {
            throw new \RuntimeException("Exchange with name '$name' not found.");
        }
    }

    public function declareExchange($name, $type, $passive, $durable, $noWait, array $arguments)
    {

    }
}
