<?php
namespace React\Amqp;

class StringStream implements ReadableInterface
{
    private $payload;

    public function __construct($payload)
    {
        $this->payload = $payload;
    }

    public function read($bytesCount)
    {
        $result = substr($this->payload, 0, $bytesCount);
        if (strlen($result) != $bytesCount) {
            throw new \OutOfBoundsException("Can't read $bytesCount bytes from string.");
        }
        $this->payload = substr($this->payload, $bytesCount);

        return $result;
    }

    public function eof()
    {
        return (strlen($this->payload) == 0);
    }
}
