<?php
namespace React\Amqp;

use Evenement\EventEmitter;
use React\Amqp\Method\ConnectionClose;
use React\Amqp\Method\ConnectionCloseOk;
use React\Amqp\Method\ConnectionOpen;
use React\Amqp\Method\ConnectionOpenOk;
use React\Amqp\Method\ConnectionStart;
use React\Amqp\Method\ConnectionStartOk;
use React\Amqp\Method\ConnectionTune;
use React\Amqp\Method\ConnectionTuneOk;
use React\Amqp\Method\MethodFactory;
use React\EventLoop\LoopInterface;
use React\Promise\Deferred;
use React\SocketClient\Connector;
use React\Amqp\Stream;

class Connection extends EventEmitter
{
    /** @var Stream */
    private $stream;

    private $loop;

    private $connector;

    private $address;

    private $port;

    private $username;

    private $password;

    private $virtualHost;

    private $locale;

    protected $connected = false;

    private $channels = array();

    private $nextAvailableChannelId = 1;

    private $channelMax = 0;

    public function __construct(LoopInterface $loop, Connector $connector)
    {
        $this->loop = $loop;
        $this->connector = $connector;
        $this->on('connection.start', array($this, 'handleConnectionStart'));
        $this->on('connection.tune', array($this, 'handleConnectionTune'));
        $this->on('connection.open-ok', array($this, 'handleConnectionOpenOk'));
        $this->on('connection.close', array($this, 'handleConnectionClose'));
    }

    public function connect($address, $port, $user, $password, $virtualHost = '/', $locale = 'en_US')
    {
        if ($this->isConnected()) {
            throw new \RuntimeException('Already connected to AMQP server.');
        }
        $this->address = $address;
        $this->port = $port;
        $this->username = $user;
        $this->password = $password;
        $this->virtualHost = $virtualHost;
        $this->locale = $locale;
        $this->connector->createSocketForAddress($address, $port)
            ->then(array($this, 'handleSocketCreate'));
    }

    public function isConnected()
    {
        return $this->connected;
    }

    public function sendFrame(Frame $frame)
    {
        $this->stream->write($frame->toBinaryString());
    }

    public function handleSocketCreate(Stream $stream)
    {
        $this->connected = true;
        $this->stream = $stream;
        $this->stream->on('data', array($this, 'handleData'));
        $this->stream->write("AMQP\x00\x00\x09\x01");
    }

    public function handleData(Stream $stream)
    {
        $frame = Frame::readFromStream($stream);
        if ($frame->getChannel() == 0) {
            switch ($frame->getType()) {
                case Frame::TYPE_METHOD:
                    $method = MethodFactory::createFromFrame($frame);
                    $this->emit($method->getName(), array($method));
                    break;
                case Frame::TYPE_HEARTBEAT:
                    $this->sendFrame($frame);
                    break;
                default:
                    throw new \RuntimeException("Can't handle frame.");
                    break;
            }
        } else {
            $channel = $this->getChannelById($frame->getChannel());
            $channel->handleFrame($frame);
        }
    }

    public function handleConnectionStart(ConnectionStart $method)
    {
        $startOk = new ConnectionStartOk($this->username, $this->password);
        $frame = Frame::createFromClientMethod($startOk);
        $this->sendFrame($frame);
    }

    public function handleConnectionTune(ConnectionTune $method)
    {
        $this->adjustChannelMaxWithServerSetting($method->getChannelMax());

        $tuneOk = new ConnectionTuneOk($this->channelMax, $method->getFrameMax(), $method->getHeartBeat());
        $frame = Frame::createFromClientMethod($tuneOk);
        $this->sendFrame($frame);

        $open = new ConnectionOpen();
        $frame = Frame::createFromClientMethod($open);
        $this->sendFrame($frame);
    }

    protected function adjustChannelMaxWithServerSetting($serverChannelMax)
    {
        if ($serverChannelMax < $this->channelMax) {
            $this->channelMax = $serverChannelMax;
        }
    }

    public function handleConnectionOpenOk(ConnectionOpenOk $method)
    {
        $this->connected = true;
        $this->emit('connected');
    }

    public function handleConnectionClose(ConnectionClose $close)
    {
        $closeOk = new ConnectionCloseOk();
        $frame = Frame::createFromClientMethod($closeOk);
        $this->sendFrame($frame);
        $this->emit('close');
    }

    public function createChannel()
    {
        $channel = new Channel($this);
        $this->channels[$channel->getId()] = $channel;

        $deferred = new Deferred();
        $channel->once('open', function($channel) use ($deferred) {
                $deferred->resolve($channel);
            });
        $channel->open();

        return $deferred->promise();
    }

    /**
     * @param $id
     * @return Channel
     * @throws \InvalidArgumentException
     */
    public function getChannelById($id)
    {
        if (!isset($this->channels[$id])) {
            throw new \InvalidArgumentException("Channel with ID $id does not exists");
        }

        return $this->channels[$id];
    }

    public function setChannelMax($value)
    {
        if ($this->isConnected()) {
            throw new \RuntimeException('Setting maximum channels while connected to the server will not affect ' .
                                        'current session. You can tune this setting only before connection is ' .
                                        'established.');
        }
        $this->channelMax = $value;
    }

    public function getNextAvailableChannelId()
    {
        if (($this->channelMax > 0) && $this->nextAvailableChannelId > $this->channelMax) {
            throw new \OutOfBoundsException("Maximum number of channels ({$this->channelMax}) per connection exceeded.");
        }

        return $this->nextAvailableChannelId++;
    }
}
