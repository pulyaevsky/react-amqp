<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;
use React\Amqp\ValueWriter;

class ChannelCloseOk implements ClientMethodInterface, ServerMethodInterface
{

    public function getName()
    {
        return 'channel.close-ok';
    }

    public function getClassId()
    {
        return 20;
    }

    public function getMethodId()
    {
        return 41;
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeShort($this->getClassId())
            ->writeShort($this->getMethodId())
            ->getResult();
    }

    public function readArguments(ValueReader $reader)
    {
        // This method has no arguments according to specs.
    }
}
