<?php
namespace React\Amqp\Method;

use React\Amqp\ValueWriter;

class ConnectionStartOk implements ClientMethodInterface
{
    private $clientProperties = array(
        'library' => array('S', 'StartupLabs\React-AMQP'),
        'library_version' => array('S', 'alpha'),
    );

    private $mechanism;

    private $response;

    private $locale;

    public function getName()
    {
        return 'connection.start-ok';
    }

    public function __construct($user, $password, $mechanism = 'AMQPLAIN', $locale = 'en_US')
    {
        $this->mechanism = $mechanism;
        $this->locale = $locale;
        $this->response = $this->rebuildResponse($user, $password);
    }

    protected function rebuildResponse($user, $password)
    {
        $writer = new ValueWriter();
        $writer->writeTable(array(
                'LOGIN' => array('S', $user),
                'PASSWORD' => array('S', $password),
            ));
        return $writer->getResult();
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeShort($this->getClassId())
            ->writeShort($this->getMethodId())

            ->writeTable($this->clientProperties)
            ->writeShortString($this->mechanism)
            ->writeRaw($this->response)
            ->writeShortString($this->locale)
            ->getResult();
    }

    public function getClassId()
    {
        return 10;
    }

    public function getMethodId()
    {
        return 11;
    }
}
