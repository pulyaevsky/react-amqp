<?php
namespace React\Amqp\Method;

use React\Amqp\ValueWriter;

class ConnectionOpen implements ClientMethodInterface
{
    private $virtualHost;

    public function __construct($virtualHost = '/')
    {
        $this->virtualHost = $virtualHost;
    }

    public function getName()
    {
        return 'connection.open';
    }

    public function getClassId()
    {
        return 10;
    }

    public function getMethodId()
    {
        return 40;
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeShort($this->getClassId())
            ->writeShort($this->getMethodId())

            ->writeShortString($this->virtualHost)
            ->writeShortString('')
            ->writeBit(false)
            ->getResult();
    }
}
