<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;
use React\Amqp\ValueWriter;

class ConnectionCloseOk implements ClientMethodInterface, ServerMethodInterface
{

    public function getName()
    {
        return 'connection.close-ok';
    }

    public function getClassId()
    {
        return 10;
    }

    public function getMethodId()
    {
        return 51;
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeShort($this->getClassId())
            ->writeShort($this->getMethodId())
            ->getResult();
    }

    public function readArguments(ValueReader $reader)
    {
        // No arguments defined for this method in AMQP spec.
    }
}
