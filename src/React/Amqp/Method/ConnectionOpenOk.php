<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;

class ConnectionOpenOk implements ServerMethodInterface
{
    private $knownHosts;

    public function getName()
    {
        return 'connection.open-ok';
    }

    public function getClassId()
    {
        return 10;
    }

    public function getMethodId()
    {
        return 41;
    }

    public function readArguments(ValueReader $reader)
    {
        $this->knownHosts = $reader->readShortString();
    }

    public function getKnownHosts()
    {
        return $this->knownHosts;
    }
}
