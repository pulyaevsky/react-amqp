<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;

class ConnectionStart implements ServerMethodInterface
{
    private $versionMajor;

    private $versionMinor;

    private $serverProperties = array();

    private $mechanisms;

    private $locales;

    public function readArguments(ValueReader $reader)
    {
        $this->versionMajor = $reader->readOctet();
        $this->versionMinor = $reader->readOctet();
        $this->serverProperties = $reader->readTable();
        $this->mechanisms = $reader->readLongString();
        $this->locales = $reader->readLongString();
    }

    public function getVersionMajor()
    {
        return $this->versionMajor;
    }

    public function getVersionMinor()
    {
        return $this->versionMinor;
    }

    public function getServerProperties()
    {
        return $this->serverProperties;
    }

    public function getMechanisms()
    {
        return $this->mechanisms;
    }

    public function getLocales()
    {
        return $this->locales;
    }

    public function getName()
    {
        return 'connection.start';
    }

    public function getClassId()
    {
        return 10;
    }

    public function getMethodId()
    {
        return 10;
    }
}
