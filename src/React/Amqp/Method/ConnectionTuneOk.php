<?php
namespace React\Amqp\Method;

use React\Amqp\ValueWriter;

class ConnectionTuneOk implements ClientMethodInterface
{
    private $channelMax;

    private $frameMax;

    private $heartBeat;

    public function __construct($channelMax, $frameMax, $heartBeat)
    {
        $this->channelMax = $channelMax;
        $this->frameMax = $frameMax;
        $this->heartBeat = $heartBeat;
    }

    public function getName()
    {
        return 'connection.tune-ok';
    }

    public function getClassId()
    {
        return 10;
    }

    public function getMethodId()
    {
        return 31;
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeShort($this->getClassId())
            ->writeShort($this->getMethodId())

            ->writeShort($this->channelMax)
            ->writeUnsignedLong($this->frameMax)
            ->writeShort($this->heartBeat)
            ->getResult();
    }
}
