<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;

class ConnectionClose implements ClientMethodInterface, ServerMethodInterface
{
    private $replyCode;

    private $replyText;

    private $failingClassId;

    private $failingMethodId;

    public function getName()
    {
        return 'connection.close';
    }

    public function getClassId()
    {
        return 10;
    }

    public function getMethodId()
    {
        return 50;
    }

    public function toBinaryString()
    {
        // TODO: Implement toBinaryString() method.
    }

    public function readArguments(ValueReader $reader)
    {
        $this->replyCode = $reader->readShort();
        $this->replyText = $reader->readShortString();
        $this->failingClassId = $reader->readShort();
        $this->failingMethodId = $reader->readShort();
    }
}
