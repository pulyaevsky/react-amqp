<?php
namespace React\Amqp\Method;

interface BaseMethodInterface
{
    public function getName();

    public function getClassId();

    public function getMethodId();
}
