<?php
namespace React\Amqp\Method;

use React\Amqp\ValueWriter;

class ChannelOpen implements ClientMethodInterface
{

    public function getName()
    {
        return 'channel.open';
    }

    public function getClassId()
    {
        return 20;
    }

    public function getMethodId()
    {
        return 10;
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeShort($this->getClassId())
            ->writeShort($this->getMethodId())

            ->writeShortString('')
            ->getResult();
    }
}
