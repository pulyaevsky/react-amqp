<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;

class ConnectionTune implements ServerMethodInterface
{
    private $channelMax;

    private $frameMax;

    private $heartBeat;

    public function getName()
    {
        return 'connection.tune';
    }

    public function getClassId()
    {
        return 10;
    }

    public function getMethodId()
    {
        return 30;
    }

    public function readArguments(ValueReader $reader)
    {
        $this->channelMax = $reader->readShort();
        $this->frameMax = $reader->readUnsignedLong();
        $this->heartBeat = $reader->readShort();
    }

    public function getChannelMax()
    {
        return $this->channelMax;
    }

    public function getFrameMax()
    {
        return $this->frameMax;
    }

    public function getHeartBeat()
    {
        return $this->heartBeat;
    }
}
