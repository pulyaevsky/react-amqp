<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;
use React\Amqp\ValueWriter;

class ChannelFlow implements ClientMethodInterface, ServerMethodInterface
{
    /** @var boolean */
    private $active;

    public function __construct($active)
    {
        $this->active = $active;
    }

    public function getName()
    {
        return 'channel.flow';
    }

    public function getClassId()
    {
        return 20;
    }

    public function getMethodId()
    {
        return 20;
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeOctet($this->getClassId())
            ->writeOctet($this->getMethodId())

            ->writeBit($this->active)
            ->getResult();
    }

    public function readArguments(ValueReader $reader)
    {
        $this->active = $reader->readBit();
    }

    public function getActive()
    {
        return $this->active;
    }
}
