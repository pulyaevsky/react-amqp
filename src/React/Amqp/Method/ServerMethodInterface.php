<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;

interface ServerMethodInterface extends BaseMethodInterface
{
    public function readArguments(ValueReader $reader);
}
