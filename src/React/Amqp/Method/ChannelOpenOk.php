<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;

class ChannelOpenOk implements ServerMethodInterface
{
    private $reserved1;

    public function getName()
    {
        return 'channel.open-ok';
    }

    public function getClassId()
    {
        return 20;
    }

    public function getMethodId()
    {
        return 11;
    }

    public function readArguments(ValueReader $reader)
    {
        $this->reserved1 = $reader->readShortString();
    }
}
