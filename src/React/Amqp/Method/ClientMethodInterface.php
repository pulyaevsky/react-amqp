<?php
namespace React\Amqp\Method;

interface ClientMethodInterface extends BaseMethodInterface
{
    public function toBinaryString();
}
