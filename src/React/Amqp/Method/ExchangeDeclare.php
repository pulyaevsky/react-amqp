<?php
namespace React\Amqp\Method;

use React\Amqp\ValueWriter;

class ExchangeDeclare implements ClientMethodInterface
{
    /** @var int */
    private $reserved1 = 0;

    /** @var string */
    private $exchangeName;

    /** @var int */
    private $type;

    /** @var bool */
    private $passive;

    /** @var bool */
    private $durable;

    /** @var bool */
    private $autoDelete; // marked as 'reserved2' in AMQP specs

    /** @var bool */
    private $internal; // marked as 'reserved3' in AMQP specs

    /** @var bool */
    private $noWait;

    /** @var array */
    private $arguments;

    public function __construct($exchangeName, $type, $passive, $durable, $noWait, $arguments, $autoDelete = true, $internal = false)
    {
        $this->exchangeName = $exchangeName;
        $this->type = $type;
        $this->passive = $passive;
        $this->durable = $durable;
        $this->noWait = $noWait;
        $this->arguments = $arguments;

        $this->autoDelete = $autoDelete;
        $this->internal = $internal;
    }

    public function getName()
    {
        return 'exchange.declare';
    }

    public function getClassId()
    {
        return 40;
    }

    public function getMethodId()
    {
        return 10;
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeShort($this->getClassId())
            ->writeShort($this->getMethodId())

            ->writeShort($this->reserved1)
            ->writeShortString($this->exchangeName)
            ->writeShortString($this->type)
            ->writeBit($this->passive)
            ->writeBit($this->durable)
            ->writeBit($this->autoDelete)
            ->writeBit($this->internal)
            ->writeBit($this->noWait)
            ->writeTable($this->arguments)
            ->getResult();
    }
}
