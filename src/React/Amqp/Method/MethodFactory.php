<?php
namespace React\Amqp\Method;

use React\Amqp\Frame;
use React\Amqp\StringStream;
use React\Amqp\ValueReader;

class MethodFactory
{
    private static $mapping = array(
        10 => array(
            10 => 'ConnectionStart',
            11 => 'ConnectionStartOk',
            20 => 'ConnectionSecure',
            21 => 'ConnectionSecureOk',
            30 => 'ConnectionTune',
            31 => 'ConnectionTuneOk',
            40 => 'ConnectionOpen',
            41 => 'ConnectionOpenOk',
            50 => 'ConnectionClose',
            51 => 'ConnectionCloseOk',
        ),
        20 => array(
            10 => 'ChannelOpen',
            11 => 'ChannelOpenOk',
            20 => 'ChannelFlow',
            21 => 'ChannelFlowOk',
            40 => 'ChannelClose',
            41 => 'ChannelCloseOk',
        ),
    );

    public static function createFromFrame(Frame $frame)
    {
        if ($frame->getType() != Frame::TYPE_METHOD) {
            throw new \InvalidArgumentException("Invalid frame type provided for Method payload.");
        }

        $payload = $frame->getPayload();
        $reader = new ValueReader(new StringStream($payload));
        $classId = $reader->readShort();
        $methodId = $reader->readShort();

        $methodClassName = "\\React\\Amqp\\Method\\". self::$mapping[$classId][$methodId];
        /** @var \React\Amqp\Method\ServerMethodInterface $method */
        $method = new $methodClassName();
        $method->readArguments($reader);

        return $method;
    }
}
