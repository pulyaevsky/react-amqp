<?php
namespace React\Amqp\Method;

use React\Amqp\ValueReader;
use React\Amqp\ValueWriter;

class ChannelClose implements ClientMethodInterface, ServerMethodInterface
{
    private $replyCode;

    private $replyText;

    private $failingClassId;

    private $failingMethodId;

    public function __construct($replyCode, $replyText, $failingClassId, $failingMethodId)
    {
        $this->replyCode = $replyCode;
        $this->replyText = $replyText;
        $this->failingClassId = $failingClassId;
        $this->failingMethodId = $failingMethodId;
    }

    public function getName()
    {
        return 'channel.close';
    }

    public function getClassId()
    {
        return 20;
    }

    public function getMethodId()
    {
        return 40;
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeShort($this->getClassId())
            ->writeShort($this->getMethodId())

            ->writeShort($this->replyCode)
            ->writeShortString($this->replyText)
            ->writeShort($this->failingClassId)
            ->writeShort($this->failingMethodId)
            ->getResult();
    }

    public function readArguments(ValueReader $reader)
    {
        $this->replyCode = $reader->readShort();
        $this->replyText = $reader->readShortString();
        $this->failingClassId = $reader->readShort();
        $this->failingMethodId = $reader->readShort();
    }
}
