<?php
namespace React\Amqp;

class UnsignedLongLong
{
    const MIN = '0';
    const MAX = '18446744073709551615';

    private $value;

    public function __construct($value)
    {
        if (self::outOfBounds($value)) {
            throw new \OutOfBoundsException("Longlong integer is out of bounds. Should be between " . self::MIN .
                " and " . self::MAX . ".");
        }
        $this->value = $value;
    }

    public static function outOfBounds($value)
    {
        return (bccomp($value, self::MIN) == -1 || bccomp($value, self::MAX) == 1);
    }

    public static function fromInt($int)
    {
        $result = (string) $int;
        if (bccomp($result, '0') == -1) {
            $adjustValue = (PHP_INT_SIZE == 8) ? self::MAX : UnsignedLong::MAX;
            $adjustValue = bcadd($adjustValue, '1');
            $result = bcadd($result, $adjustValue);
        }

        return new self($result);
    }

    public static function fromHiAndLoWords($hi, $lo)
    {
        $value = bcadd(bcmul($hi, "4294967296"), $lo);

        return new self($value);
    }

    public function __toString()
    {
        return $this->value;
    }

    public function toBinaryString()
    {
        $bytesArray = $this->splitIntoBytes();
        return implode('', array_map('chr', $bytesArray));
    }

    private function splitIntoBytes()
    {
        $value = $this->value;
        $res = array();
        $bytesCount = 8;
        while ($bytesCount > 0) {
            $b = bcmod($value,'256');
            $res[] = (int) $b;
            $value = bcdiv($value,'256', 0);
            $bytesCount--;
        }

        return array_reverse($res);
    }
}
