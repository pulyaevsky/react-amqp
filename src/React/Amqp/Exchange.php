<?php
namespace React\Amqp;

use React\Amqp\Method\ExchangeDeclare;
use React\Promise\Deferred;

class Exchange
{
    const TYPE_DIRECT  = 'amq.direct';
    const TYPE_FANOUT  = 'amq.fanout';
    const TYPE_TOPIC   = 'amq.topic';
    const TYPE_HEADERS = 'amq.match';

    private $name;

    private $connection;

    private $channel;

    public function __construct($name, Connection $connection, Channel $channel)
    {
        $this->name = $name;
        $this->connection = $connection;
        $this->channel = $channel;
    }

    public function declareExchange($type, $passive, $durable, $noWait, $arguments)
    {
        $deferred = new Deferred();

        $declareMethod = new ExchangeDeclare($this->name, $type, $passive, $durable, $noWait, $arguments);

        $frame = Frame::createFromClientMethod($declareMethod, $this->channel->getId());
        $this->connection->sendFrame($frame);

        return $deferred->promise();
    }
}
