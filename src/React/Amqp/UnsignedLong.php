<?php


namespace React\Amqp;

/**
 * Representation of unsigned 32 bit integer
 */
class UnsignedLong
{
    const MIN = '0';
    const MAX = '4294967295';

    /** @var string Value stored as a string */
    private $value;

    public function __construct($value)
    {
        if (self::outOfBounds($value)) {
            throw new \OutOfBoundsException("Value '$value' is out of bounds for unsigned long.");
        }

        $this->value = (string) $value;
    }

    public static function outOfBounds($value)
    {
        if (bccomp($value, self::MIN) == -1 || bccomp($value, self::MAX) == 1) {
            return true;
        }

        return false;
    }

    public static function fromInt($int)
    {
        if (SignedLong::outOfBounds($int)) {
            throw new \OutOfBoundsException("Integer '$int' is out of bounds. Must be between " . SignedLong::MIN .
                " and " . SignedLong::MAX . ".");
        }

        if ($int < 0) {
            $int = self::convertNegativeIntToUnsigned($int);
        }
        return new self($int);
    }

    /**
     * @param int $value
     * @return string
     */
    public static function convertNegativeIntToUnsigned($value)
    {
        $adjustValue = bcadd(self::MAX, '1');
        $result = bcadd($value, $adjustValue);

        return $result;
    }

    public function __toString()
    {
        return $this->value;
    }

    public function toBinaryString()
    {
        $bytesArray = $this->splitIntoBytes();
        return implode('', array_map('chr', $bytesArray));
    }

    private function splitIntoBytes()
    {
        $value = $this->value;
        $res = array();
        $bytesCount = 4;
        while ($bytesCount > 0) {
            $b = bcmod($value,'256');
            $res[] = (int) $b;
            $value = bcdiv($value,'256', 0);
            $bytesCount--;
        }

        return array_reverse($res);
    }
}
