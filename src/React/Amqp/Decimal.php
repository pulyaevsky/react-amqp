<?php
namespace React\Amqp;

class Decimal
{
    private $value;

    private $scale;

    public function __construct($value, $scale)
    {
        if ($scale < 0) {
            throw new \OutOfBoundsException("Number of decimal places must be positive value.");
        }
        $this->value = $value;
        $this->scale = $scale;
    }

    public function __toString()
    {
        return bcdiv($this->value, bcpow(10, $this->scale), $this->scale);
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer
            ->writeOctet($this->scale)
            ->writeSignedLong($this->value)
            ->getResult();
    }
}
