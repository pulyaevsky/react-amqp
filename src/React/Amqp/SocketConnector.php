<?php


namespace React\Amqp;


use React\Dns\Resolver\Resolver;
use React\EventLoop\LoopInterface;
use React\SocketClient\Connector;

class SocketConnector extends Connector
{
    private $loop;

    public function __construct(LoopInterface $loop, Resolver $resolver)
    {
        parent::__construct($loop, $resolver);
        $this->loop = $loop;
    }

    public function handleConnectedSocket($socket)
    {
        return new Stream($socket, $this->loop);
    }
}
