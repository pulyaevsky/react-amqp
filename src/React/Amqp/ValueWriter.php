<?php
namespace React\Amqp;


class ValueWriter
{
    private $value;

    private $bitsBuffer = array();

    private $bitsCount = 0;

    /**
     * Write an AMQP 'bit' data type.
     * Consequent bits can be packed into one or more bytes, so here we don't directly append bit to the result,
     * but save it into special buffer (array of bytes) which will be flushed before first non-bit value to be written.
     *
     * @param mixed $value
     * @return ValueWriter
     */
    public function writeBit($value)
    {
        $value = $value ? 1 : 0;
        $shift = $this->bitsCount % 8;
        $currentByte = ($shift == 0) ? 0 : array_pop($this->bitsBuffer);
        $currentByte |= ($value << $shift);
        array_push($this->bitsBuffer, $currentByte);
        $this->bitsCount++;

        return $this;
    }

    protected function flushBits()
    {
        if (!empty($this->bitsBuffer)) {
            $this->value .= implode('', array_map('chr', $this->bitsBuffer));
            $this->bitsBuffer = array();
            $this->bitsCount = 0;
        }
    }

    /**
     * Write an AMQP 'octet' value.
     *
     * @param int $value
     * @return ValueWriter
     * @throws \OutOfBoundsException
     */
    public function writeOctet($value)
    {
        if ($value < 0 || $value > 255) {
            throw new \OutOfBoundsException('Octet value is out of bounds. Should be 0..255.');
        }
        $this->flushBits();
        $this->value .= chr($value);

        return $this;
    }

    /**
     * Write an AMQP 'short' value.
     *
     * @param int $value
     * @return ValueWriter
     * @throws \OutOfBoundsException
     */
    public function writeShort($value)
    {
        if ($value < 0 || $value > 65535) {
            throw new \OutOfBoundsException('Short int value is out of bounds. Should be 0..65535.');
        }
        $this->flushBits();
        $this->value .= pack('n', $value);

        return $this;
    }

    /**
     * Write an AMQP 'long integer' (32 bits, unsigned).
     *
     * @param integer|string|UnsignedLong $value
     * @return ValueWriter
     * @throws \InvalidArgumentException
     */
    public function writeUnsignedLong($value)
    {
        $this->flushBits();

        if (is_int($value)) {
            $value = UnsignedLong::fromInt($value);
        } elseif (is_string($value) && is_numeric($value)) {
            $value = new UnsignedLong($value);
        }

        if (! $value instanceof UnsignedLong) {
            throw new \InvalidArgumentException("Invalid argument type provided for ValueWriter::writeLong(). " .
            "Must be an integer, a numeric string or an instance of React\\Amqp\\UnsignedLong");
        }

        $this->value .= $value->toBinaryString();

        return $this;
    }

    public function writeUnsignedLongLong($value)
    {
        $this->flushBits();

        if (is_int($value)) {
            $value = UnsignedLongLong::fromInt($value);
        } else if (is_string($value) && is_numeric($value)) {
            $value = new UnsignedLongLong($value);
        }

        if (! $value instanceof UnsignedLongLong) {
            throw new \InvalidArgumentException("Invalid argument type provided for ValueWriter::writeLongLong(). " .
            "Must be an integer, a numeric string or an instance of React\\Amqp\\UnsignedLongLong");
        }
        $this->value .= $value->toBinaryString();

        return $this;
    }

    public function writeShortString($value)
    {
        $this->flushBits();

        if (strlen($value) > 255) {
            throw new \OutOfBoundsException("Short strings must be between 0 to 255 characters in length.");
        }

        $this->writeOctet(strlen($value));
        $this->value .= $value;

        return $this;
    }

    public function writeLongString($value)
    {
        $this->flushBits();

        $this->writeUnsignedLong(strlen($value));
        $this->value .= $value;

        return $this;
    }

    public function writeSignedLong($value)
    {
        $this->flushBits();
        $this->value .= pack('N', $value);

        return $this;
    }

    public function writeTable(array $value)
    {
        $writer = new self();
        foreach ($value as $name => $data) {
            $writer->writeShortString($name);

            list($type, $fieldValue) = $data;
            switch ($type) {
                case 'S':
                    $writer
                        ->writeRaw($type)
                        ->writeLongString($fieldValue);
                    break;
                case 'I':
                    $writer
                        ->writeRaw($type)
                        ->writeSignedLong($fieldValue);
                    break;
                case 'D':
                    $writer
                        ->writeRaw($type)
                        ->writeDecimal($fieldValue);
                    break;
                case 'T':
                    $writer->writeRaw($type);
                    $writer->writeTimestamp($fieldValue);
                    break;
                case 'V':
                    $writer->writeRaw($type);
                    break;
                default:
                    throw new \RuntimeException("Can't write table. Invalid field type provided: $type.");
            }
        }

        $this->writeLongString($writer->getResult());

        return $this;
    }

    public function writeRaw($value)
    {
        $this->flushBits();
        $this->value .= $value;

        return $this;
    }

    public function writeDecimal(Decimal $value)
    {
        $this->flushBits();
        $this->value .= $value->toBinaryString();

        return $this;
    }

    public function writeTimestamp($value)
    {
        return $this->writeUnsignedLongLong($value);
    }

    public function getResult()
    {
        $this->flushBits();

        return $this->value;
    }
}
