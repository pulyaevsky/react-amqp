<?php
namespace React\Amqp;

use React\Amqp\Method\ClientMethodInterface;
use React\Amqp\Stream;

class Frame
{
    const FRAME_END = 0xCE;

    const TYPE_METHOD     = 1;
    const TYPE_HEADER     = 2;
    const TYPE_BODY       = 3;
    const TYPE_OOB_METHOD = 4;
    const TYPE_OOB_HEADER = 5;
    const TYPE_OOB_BODY   = 6;
    const TYPE_TRACE      = 7;
    const TYPE_HEARTBEAT  = 8;
    const TYPE_REQUEST    = 9;
    const TYPE_RESPONSE   = 10;

    /** @var int */
    private $type;

    /** @var int */
    private $channel;

    /** @var int */
    private $size;

    /** @var string */
    private $payload;

    public function __construct($type, $channel, $payload)
    {
        $this->type = $type;
        $this->channel = $channel;
        $this->size = strlen($payload);
        $this->payload = $payload;
    }

    public static function readFromStream(Stream $stream)
    {
        $reader = new ValueReader($stream);
        $type = $reader->readOctet();
        $channel = $reader->readShort();
        $size = $reader->readUnsignedLong();
        $payload = $stream->read((string) $size);
        $end = $reader->readOctet();
        if ($end != self::FRAME_END) {
            throw new \RuntimeException("Invalid frame.");
        }
        return new self($type, $channel, $payload);
    }

    public static function createFromClientMethod(ClientMethodInterface $method, $channel = 0)
    {
        $payload = $method->toBinaryString();

        return new self(self::TYPE_METHOD, $channel, $payload);
    }

    public function toBinaryString()
    {
        $writer = new ValueWriter();

        return $writer->writeOctet($this->type)
            ->writeShort($this->channel)
            ->writeUnsignedLong($this->size)
            ->writeRaw($this->payload)
            ->writeOctet(self::FRAME_END)
            ->getResult();
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function setPayload($payload)
    {
        $this->payload = $payload;
        $this->size = strlen($payload);

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getChannel()
    {
        return $this->channel;
    }

    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    public function getSize()
    {
        return $this->size;
    }
}
