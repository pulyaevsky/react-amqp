<?php
namespace React\Amqp;

class ValueReader
{
    private $bitsCount = 0;

    private $bitsBuffer;

    private $stream;

    public function __construct(ReadableInterface $stream)
    {
        $this->stream = $stream;
    }

    /**
     * Read an AMQP 'bit' data type from stream.
     *
     * Bits are normally accumulated into whole octets in AMQP.
     * However if two or more bits are contiguous in a frame these will be packed into one or more octets.
     */
    public function readBit()
    {
        if ($this->bitsCount == 0) {
            $this->bitsBuffer = ord($this->stream->read(1));
            $this->bitsCount = 8;
        }

        $result = ($this->bitsBuffer & 1);
        $this->bitsBuffer >>= 1;
        $this->bitsCount--;

        return $result;
    }

    /**
     * Read an AMQP 'octet' data type from stream. Octets are 8 bits in size.
     */
    public function readOctet()
    {
        $this->resetBitsBuffer();
        $result = unpack('C', $this->stream->read(1));

        return array_shift($result);
    }

    protected function resetBitsBuffer()
    {
        $this->bitsCount = 0;
        $this->bitsBuffer = null;
    }

    /**
     * Read an AMQP 'short integer' data type (16 bits, unsigned).
     */
    public function readShort()
    {
        $this->resetBitsBuffer();
        $result = unpack('n', $this->stream->read(2));

        return array_shift($result);
    }

    /**
     * Read an AMQP 'long integer' data type (32 bits, unsigned).
     *
     * Since PHP does not have unsigned 32 bit integer we have to convert result to a string.
     *
     * @return string
     */
    public function readUnsignedLong()
    {
        $this->resetBitsBuffer();
        $result = unpack('N', $this->stream->read(4));
        $value = array_shift($result);

        return new UnsignedLong($value);
    }

    public function readSignedLong()
    {
        $this->resetBitsBuffer();
        $result = unpack('N', $this->stream->read(4));
        $result = array_shift($result);
        // Unpack() returns unsigned 32 bit integer value. We should check for negative range manually here.
        if ($result > SignedLong::MAX) {
            $result = $result - UnsignedLong::MAX - 1;
        }

        return (int) $result;
    }

    public function readUnsignedLongLong()
    {
        $this->resetBitsBuffer();
        $hiWord = $this->readUnsignedLong();
        $loWord = $this->readUnsignedLong();

        return UnsignedLongLong::fromHiAndLoWords($hiWord, $loWord);
    }

    public function readTimestamp()
    {
        return $this->readUnsignedLongLong();
    }

    public function readDecimal()
    {
        $this->resetBitsBuffer();
        $scale = $this->readOctet();
        $value = $this->readSignedLong();

        return new Decimal($value, $scale);
    }

    public function readShortString()
    {
        $this->resetBitsBuffer();
        $length = $this->readOctet();

        return $this->stream->read($length);
    }

    public function readLongString()
    {
        $this->resetBitsBuffer();
        $length = (string) $this->readUnsignedLong();

        return $this->stream->read($length);
    }

    public function readTable()
    {
        $this->resetBitsBuffer();

        $result = array();
        $payload = $this->readLongString();
        $stream = new StringStream($payload);
        $reader = new ValueReader($stream);
        while (!$stream->eof()) {
            $name = $reader->readShortString();
            $type = chr($reader->readOctet());
            $value = $reader->readTableValue($type);

            if (!isset($result[$name])) {
                $result[$name] = array($type, $value);
            }
        }

        return $result;
    }

    protected function readTableValue($type)
    {
        // @todo Consider how to replace this with polymorphism
        switch ($type) {
            case 'S':
                $value = $this->readLongString();
                break;
            case 'I':
                $value = $this->readSignedLong();
                break;
            case 'D':
                $value = $this->readDecimal();
                break;
            case 'T':
                $value = $this->readTimestamp();
                break;
            case 'F':
                $value = $this->readTable();
                break;
            case 'V':
                // 'void' type, assume it's null, however there is no any description of what it should contain in the specs.
                // assume it should contain nothing
                $value = null;
                break;

            // Field types below are not defined in AMQP specs
            case 't':
                $value = $this->readOctet();
                break;
            case 'l':
                $value = $this->readUnsignedLongLong();
                break;

            default:
                throw new \InvalidArgumentException("Invalid table field type provided: '$type'.");
                break;
        }

        return $value;
    }
}
