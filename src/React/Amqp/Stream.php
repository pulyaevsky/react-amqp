<?php
namespace React\Amqp;

use React\Stream\Stream as BaseStream;

class Stream extends BaseStream implements ReadableInterface
{
    public function read($bytesCount)
    {
        $result = '';
        $read = 0;

        while ($read < $bytesCount &&
            !feof($this->stream) &&
            (false !== ($buf = fread($this->stream, $bytesCount - $read)))) {

            $read += strlen($buf);
            $result .= $buf;
        }

        if (strlen($result) != $bytesCount) {
            throw new \RuntimeException("Error reading data. Received " .
            strlen($result) . " instead of expected $bytesCount bytes");
        }

        return $result;
    }

    public function handleData($stream)
    {
        $this->emit('data', array($this));

//        if (!is_resource($stream) || feof($stream)) {
//            $this->end();
//        }
    }

    public function eof()
    {
        return feof($this->stream);
    }
}
